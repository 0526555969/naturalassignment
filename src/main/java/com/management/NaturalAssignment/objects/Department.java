package com.management.NaturalAssignment.objects;

public class Department implements Name {

	private Long startDate = System.currentTimeMillis();
	private Long endDate;
	private String name;

	public Department(String name) {
		this.name = name;
	}

	@Override
	public String entityName() {
		return this.name;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Department [startDate=" + startDate + ", endDate=" + endDate + ", name=" + name + "]";
	}

}
