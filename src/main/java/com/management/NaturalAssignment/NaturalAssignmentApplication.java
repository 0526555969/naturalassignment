package com.management.NaturalAssignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NaturalAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(NaturalAssignmentApplication.class, args);
	}

}
