package com.management.NaturalAssignment.objects;

import java.util.List;

public class Employee implements Name {

	private String name;
	private Department department;
	private int id;
	private Employee manager;
	private List<Project> projectList;

	public Employee(int id, String name, String deptName, Employee manager) {
		this.id = id;
		this.name = name;
		this.manager = manager;
		this.department = new Department(deptName);
	}

	@Override
	public String entityName() {
		return this.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", department=" + department + ", id=" + id + ", manager=" + manager
				+ ", projectList=" + projectList + "]";
	}

}
