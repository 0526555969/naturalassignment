package com.management.NaturalAssignment.objects;

import java.util.ArrayList;
import java.util.List;

public class Project implements Name {

	private double budget;
	private final String projectName;
	private Employee leader;
	private List<Employee> employeeList = new ArrayList<>();
	private Long startDate = System.currentTimeMillis();
	private Long endDate;
	private int id;

	public Project(int id, String pName, double budget, Employee leader) {
		this.budget = budget;
		this.leader = leader;
		this.projectName = pName;
		this.id = id;
	}

	@Override
	public String entityName() {
		return this.projectName;
	}

	public String getProjectName() {
		return projectName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void finishProject() {
		endDate = System.currentTimeMillis();
	}

	public double getBudget() {
		return budget;
	}

	public void setBudget(double budget) {
		this.budget = budget;
	}

	public void addEmployee(Employee e) {
		if (!this.employeeList.contains(e)) {
			this.employeeList.add(e);
		}
	}

	public int numOfParticipants() {
		return this.employeeList.size();
	}

	public String getFirstEmployeeName() {
		if (this.employeeList.size() > 0) {
			return this.employeeList.get(0).getName();
		}
		return "";
	}

	public String getSecondEmployeeName() {
		if (this.employeeList.size() > 1) {
			return this.employeeList.get(1).getName();
		}
		return "";
	}

	@Override
	public String toString() {
		return "Project [budget=" + budget + ", projectName=" + projectName + ", leader=" + leader + ", employeeList="
				+ employeeList + ", startDate=" + startDate + ", endDate=" + endDate + ", id=" + id + "]";
	}

}
