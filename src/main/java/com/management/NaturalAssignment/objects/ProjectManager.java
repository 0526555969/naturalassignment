package com.management.NaturalAssignment.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manager")
public class ProjectManager {

	private Map<Integer, Project> projects = new HashMap<>();
	private Map<Integer, Employee> employees = new HashMap<>();
	// TODO Employee should be under department and not as implemented
	private Map<Integer, Department> department = new HashMap<>();
	private List<Project> maxBudgetProjects = new ArrayList<>();
	private List<Project> minBudgetProjects = new ArrayList<>();
	private double avgBudget;

	public ProjectManager() {
		Employee ceo = new Employee(0, "CEO", "General", null);
		employees.put(ceo.getId(), ceo);
	}

	@GetMapping("/createNewProject/{projName}/{budget}/{leaderId}")
	public String addProject(@PathVariable("projName") final String projName,
			@PathVariable("budget") final double budget, @PathVariable("leaderId") final int leaderId) {
		Employee leader = employees.get(leaderId);
		if (leader == null) {
			return "No employee exist with id: " + leaderId;
		}
		Project project = new Project(projects.size(), projName, budget, leader);
		project.addEmployee(leader);
		this.avgBudget = (this.avgBudget * projects.size() + project.getBudget()) / (projects.size() + 1);
		this.projects.put(project.getId(), project);
		manageMaxBudget(project);
		manageMinBudget(project);
		return "Welcome project " + project;
	}

	@GetMapping("/createNewEmployee/{empName}/{deptName}/{managerId}")
	public String createEmployee(@PathVariable("empName") final String empName,
			@PathVariable("deptName") final String deptName, @PathVariable("managerId") final int managerId) {
		Employee manager = this.employees.get(managerId);
		if (manager == null) {
			return "No such manager id: " + managerId;
		}
		Employee e = new Employee(this.employees.size(), empName, deptName, manager);
		employees.put(e.getId(), e);
		return "welcome employee: " + e;
	}

	@GetMapping("/addEmployeeToProject/{empId}/{pId}")
	public String addEmployeeToProject(@PathVariable("empId") final int empId, @PathVariable("pId") final int pId) {
		Project p = projects.get(pId);
		Employee e = employees.get(empId);
		p.addEmployee(e);
		return "Added employee " + e.getName() + " to project " + p.getProjectName();
	}

	@GetMapping("/getHighestBudget")
	public double getHighestBudget() {
		if (!this.maxBudgetProjects.isEmpty()) {
			return this.maxBudgetProjects.get(0).getBudget();
		}
		return 0.0;
	}

	@GetMapping("/getMinBudget")
	public double getMinBudget() {
		if (!this.minBudgetProjects.isEmpty()) {
			return this.minBudgetProjects.get(0).getBudget();
		}
		return 0.0;
	}

	@GetMapping("/getAvgBudget")
	public double getAvgBudget() {
		return avgBudget;
	}

	@GetMapping("/getGeneralProjectName/{pId}")
	public String getGeneralProjectName(@PathVariable("pId") final int pId) {
		Project p = projects.get(pId);
		return p.getName();
	}

	@GetMapping("/getGeneralEmployeeName")
	public String getGeneralEmployeeName(@PathVariable("empId") final int empId) {
		Employee e = employees.get(empId);
		return e.getName();
	}

	@GetMapping("/getHighestBudgetProjects")
	public String getHighestBudgetProjects() {
		StringBuilder sb = new StringBuilder();
		maxBudgetProjects.forEach(p -> {
			sb.append(p.getProjectName() + " ");
		});
		return "Highest budget projects are: " + sb;
	}

	@GetMapping("/getLowestBudgetProjects")
	public String getLowestBudgetProjects() {
		StringBuilder sb = new StringBuilder();
		minBudgetProjects.forEach(p -> {
			sb.append(p.getProjectName() + " ");
		});
		return "Lowest budget projects are: " + sb;
	}

	@GetMapping("/findEmpNameOfHighestParticpiantProjects")
	public String findEmpNameOfHighestParticpiantProjects() {
		Project highest = null;
		int maxParticipants = 0;
		for (Project p : this.projects.values()) {
			if (p.numOfParticipants() >= maxParticipants) {
				highest = p;
			}
		}
		return "2 employees that work in the project with the highest number of participants: "
				+ highest.getFirstEmployeeName() + " , " + highest.getSecondEmployeeName();
	}

	// TODO not thread safe yet
	private void manageMaxBudget(Project p) {
		if (maxBudgetProjects.isEmpty()) {
			maxBudgetProjects.add(p);
		} else {
			if (p.getBudget() > maxBudgetProjects.get(0).getBudget()) {
				maxBudgetProjects = new ArrayList<>();
				maxBudgetProjects.add(p);
			} else if (p.getBudget() == maxBudgetProjects.get(0).getBudget()) {
				maxBudgetProjects.add(p);
			}
		}
	}

	// TODO not thread safe yet
	private void manageMinBudget(Project p) {
		if (minBudgetProjects.isEmpty()) {
			minBudgetProjects.add(p);
		} else {
			if (p.getBudget() < minBudgetProjects.get(0).getBudget()) {
				minBudgetProjects = new ArrayList<>();
				minBudgetProjects.add(p);
			} else if (p.getBudget() == minBudgetProjects.get(0).getBudget()) {
				minBudgetProjects.add(p);
			}
		}
	}

}
