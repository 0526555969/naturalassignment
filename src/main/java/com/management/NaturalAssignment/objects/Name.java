package com.management.NaturalAssignment.objects;

public interface Name {

	default String getName() {
		return "amazing but: " + entityName() + " is the general name";
	}


	abstract String entityName();
}

